﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Penggajaian
{
    public partial class Menu : MaterialForm
    {
        public Menu()
        {
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            InitializeComponent();

            // Create a material theme manager and add the form to manage (this)
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;

            // Configure color schema
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue400, Primary.Blue500,
                Primary.Blue500, Accent.LightBlue200,
                TextShade.WHITE
            );
        }

        private void MaterialRaisedButton1_Click(object sender, EventArgs e)
        {

        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }


        private void materialButton4_Click(object sender, EventArgs e)
        {
            Karyawan ky = new Karyawan();
            this.Hide();
            ky.ShowDialog();
            this.Close();
        }

        private void materialButton5_Click(object sender, EventArgs e)
        {
            Jabatan jb = new Jabatan();
            this.Hide();
            jb.ShowDialog();
            this.Close();
        }

        private void materialButton6_Click(object sender, EventArgs e)
        {
            Gaji gj = new Gaji();
            this.Hide();
            gj.ShowDialog();
            this.Close();
        }

        private void materialFloatingActionButton2_Click(object sender, EventArgs e)
        {
            Menu mn = new Menu();
            this.Hide();
            mn.ShowDialog();
            this.Close();
        }

        private void MaterialButton7_Click(object sender, EventArgs e)
        {
            ReportGajiKaryawan rgk = new ReportGajiKaryawan();
            rgk.ShowDialog();
        }
    }
}
