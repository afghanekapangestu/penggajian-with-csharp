﻿namespace Penggajaian
{
    partial class Jabatan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialFloatingActionButton1 = new MaterialSkin.Controls.MaterialFloatingActionButton();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialListView1 = new MaterialSkin.Controls.MaterialListView();
            this.txtNamaJabatan = new MaterialSkin.Controls.MaterialTextBox();
            this.txtJumlahGajiPokok = new MaterialSkin.Controls.MaterialTextBox();
            this.txtUpahLembur = new MaterialSkin.Controls.MaterialTextBox();
            this.materialButton1 = new MaterialSkin.Controls.MaterialButton();
            this.materialButton2 = new MaterialSkin.Controls.MaterialButton();
            this.materialButton3 = new MaterialSkin.Controls.MaterialButton();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.txtID = new MaterialSkin.Controls.MaterialTextBox();
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nama_jabatan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.jumlah_gaji = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.upah_lembur = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // materialFloatingActionButton1
            // 
            this.materialFloatingActionButton1.AnimateShowHideButton = false;
            this.materialFloatingActionButton1.Depth = 0;
            this.materialFloatingActionButton1.DrawShadows = true;
            this.materialFloatingActionButton1.Icon = global::Penggajaian.Properties.Resources.icons8_arrow_pointing_left_96;
            this.materialFloatingActionButton1.Location = new System.Drawing.Point(698, 370);
            this.materialFloatingActionButton1.Mini = false;
            this.materialFloatingActionButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialFloatingActionButton1.Name = "materialFloatingActionButton1";
            this.materialFloatingActionButton1.Size = new System.Drawing.Size(56, 56);
            this.materialFloatingActionButton1.TabIndex = 3;
            this.materialFloatingActionButton1.Text = "Back\r\n";
            this.materialFloatingActionButton1.UseVisualStyleBackColor = true;
            this.materialFloatingActionButton1.Click += new System.EventHandler(this.MaterialFloatingActionButton1_Click);
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel1.Location = new System.Drawing.Point(65, 330);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(107, 19);
            this.materialLabel1.TabIndex = 4;
            this.materialLabel1.Text = "Nama Jabatan";
            this.materialLabel1.Click += new System.EventHandler(this.MaterialLabel1_Click);
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel2.Location = new System.Drawing.Point(65, 382);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(135, 19);
            this.materialLabel2.TabIndex = 5;
            this.materialLabel2.Text = "Jumlah Gaji Pokok";
            this.materialLabel2.Click += new System.EventHandler(this.MaterialLabel2_Click);
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel3.Location = new System.Drawing.Point(65, 429);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(96, 19);
            this.materialLabel3.TabIndex = 6;
            this.materialLabel3.Text = "Upah Lembur";
            // 
            // materialListView1
            // 
            this.materialListView1.AutoSizeTable = false;
            this.materialListView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.materialListView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.nama_jabatan,
            this.jumlah_gaji,
            this.upah_lembur});
            this.materialListView1.Depth = 0;
            this.materialListView1.FullRowSelect = true;
            this.materialListView1.HideSelection = false;
            this.materialListView1.Location = new System.Drawing.Point(163, 85);
            this.materialListView1.MinimumSize = new System.Drawing.Size(200, 100);
            this.materialListView1.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialListView1.MouseState = MaterialSkin.MouseState.OUT;
            this.materialListView1.Name = "materialListView1";
            this.materialListView1.OwnerDraw = true;
            this.materialListView1.Size = new System.Drawing.Size(495, 160);
            this.materialListView1.TabIndex = 7;
            this.materialListView1.UseCompatibleStateImageBehavior = false;
            this.materialListView1.View = System.Windows.Forms.View.Details;
            this.materialListView1.SelectedIndexChanged += new System.EventHandler(this.MaterialListView1_SelectedIndexChanged);
            // 
            // txtNamaJabatan
            // 
            this.txtNamaJabatan.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNamaJabatan.Depth = 0;
            this.txtNamaJabatan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtNamaJabatan.Location = new System.Drawing.Point(247, 313);
            this.txtNamaJabatan.MaxLength = 50;
            this.txtNamaJabatan.MouseState = MaterialSkin.MouseState.OUT;
            this.txtNamaJabatan.Multiline = false;
            this.txtNamaJabatan.Name = "txtNamaJabatan";
            this.txtNamaJabatan.Size = new System.Drawing.Size(185, 36);
            this.txtNamaJabatan.TabIndex = 11;
            this.txtNamaJabatan.Text = "";
            this.txtNamaJabatan.UseTallSize = false;
            // 
            // txtJumlahGajiPokok
            // 
            this.txtJumlahGajiPokok.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtJumlahGajiPokok.Depth = 0;
            this.txtJumlahGajiPokok.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtJumlahGajiPokok.Location = new System.Drawing.Point(247, 365);
            this.txtJumlahGajiPokok.MaxLength = 50;
            this.txtJumlahGajiPokok.MouseState = MaterialSkin.MouseState.OUT;
            this.txtJumlahGajiPokok.Multiline = false;
            this.txtJumlahGajiPokok.Name = "txtJumlahGajiPokok";
            this.txtJumlahGajiPokok.Size = new System.Drawing.Size(185, 36);
            this.txtJumlahGajiPokok.TabIndex = 12;
            this.txtJumlahGajiPokok.Text = "";
            this.txtJumlahGajiPokok.UseTallSize = false;
            // 
            // txtUpahLembur
            // 
            this.txtUpahLembur.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUpahLembur.Depth = 0;
            this.txtUpahLembur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtUpahLembur.Location = new System.Drawing.Point(247, 412);
            this.txtUpahLembur.MaxLength = 50;
            this.txtUpahLembur.MouseState = MaterialSkin.MouseState.OUT;
            this.txtUpahLembur.Multiline = false;
            this.txtUpahLembur.Name = "txtUpahLembur";
            this.txtUpahLembur.Size = new System.Drawing.Size(185, 36);
            this.txtUpahLembur.TabIndex = 13;
            this.txtUpahLembur.Text = "";
            this.txtUpahLembur.UseTallSize = false;
            // 
            // materialButton1
            // 
            this.materialButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton1.Depth = 0;
            this.materialButton1.DrawShadows = true;
            this.materialButton1.HighEmphasis = true;
            this.materialButton1.Icon = null;
            this.materialButton1.Location = new System.Drawing.Point(483, 274);
            this.materialButton1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton1.Name = "materialButton1";
            this.materialButton1.Size = new System.Drawing.Size(77, 36);
            this.materialButton1.TabIndex = 14;
            this.materialButton1.Text = "Simpan";
            this.materialButton1.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton1.UseAccentColor = false;
            this.materialButton1.UseVisualStyleBackColor = true;
            this.materialButton1.Click += new System.EventHandler(this.MaterialButton1_Click);
            // 
            // materialButton2
            // 
            this.materialButton2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton2.Depth = 0;
            this.materialButton2.DrawShadows = true;
            this.materialButton2.HighEmphasis = true;
            this.materialButton2.Icon = null;
            this.materialButton2.Location = new System.Drawing.Point(581, 274);
            this.materialButton2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton2.Name = "materialButton2";
            this.materialButton2.Size = new System.Drawing.Size(69, 36);
            this.materialButton2.TabIndex = 15;
            this.materialButton2.Text = "Hapus";
            this.materialButton2.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton2.UseAccentColor = false;
            this.materialButton2.UseVisualStyleBackColor = true;
            this.materialButton2.Click += new System.EventHandler(this.MaterialButton2_Click);
            // 
            // materialButton3
            // 
            this.materialButton3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton3.Depth = 0;
            this.materialButton3.DrawShadows = true;
            this.materialButton3.HighEmphasis = true;
            this.materialButton3.Icon = null;
            this.materialButton3.Location = new System.Drawing.Point(668, 274);
            this.materialButton3.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton3.Name = "materialButton3";
            this.materialButton3.Size = new System.Drawing.Size(73, 36);
            this.materialButton3.TabIndex = 16;
            this.materialButton3.Text = "update";
            this.materialButton3.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton3.UseAccentColor = false;
            this.materialButton3.UseVisualStyleBackColor = true;
            this.materialButton3.Click += new System.EventHandler(this.MaterialButton3_Click);
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel4.Location = new System.Drawing.Point(65, 284);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(79, 19);
            this.materialLabel4.TabIndex = 17;
            this.materialLabel4.Text = "ID Jabatan";
            // 
            // txtID
            // 
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtID.Depth = 0;
            this.txtID.Enabled = false;
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txtID.Location = new System.Drawing.Point(247, 267);
            this.txtID.MaxLength = 50;
            this.txtID.MouseState = MaterialSkin.MouseState.OUT;
            this.txtID.Multiline = false;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(185, 36);
            this.txtID.TabIndex = 18;
            this.txtID.Text = "";
            this.txtID.UseTallSize = false;
            this.txtID.TextChanged += new System.EventHandler(this.MaterialTextBox3_TextChanged);
            // 
            // ID
            // 
            this.ID.Text = "ID";
            // 
            // nama_jabatan
            // 
            this.nama_jabatan.Text = "Nama Jabatan";
            this.nama_jabatan.Width = 140;
            // 
            // jumlah_gaji
            // 
            this.jumlah_gaji.Text = "Jumlah Gaji Pokok";
            this.jumlah_gaji.Width = 160;
            // 
            // upah_lembur
            // 
            this.upah_lembur.Text = "Upah Lembur";
            this.upah_lembur.Width = 120;
            // 
            // Jabatan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 476);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.materialButton3);
            this.Controls.Add(this.materialButton2);
            this.Controls.Add(this.materialButton1);
            this.Controls.Add(this.txtUpahLembur);
            this.Controls.Add(this.txtJumlahGajiPokok);
            this.Controls.Add(this.txtNamaJabatan);
            this.Controls.Add(this.materialListView1);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.materialFloatingActionButton1);
            this.Name = "Jabatan";
            this.Text = "Jabatan";
            this.Load += new System.EventHandler(this.Jabatan_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFloatingActionButton materialFloatingActionButton1;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialListView materialListView1;
        private MaterialSkin.Controls.MaterialTextBox txtNamaJabatan;
        private MaterialSkin.Controls.MaterialTextBox txtJumlahGajiPokok;
        private MaterialSkin.Controls.MaterialTextBox txtUpahLembur;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader nama_jabatan;
        private System.Windows.Forms.ColumnHeader jumlah_gaji;
        private System.Windows.Forms.ColumnHeader upah_lembur;
        private MaterialSkin.Controls.MaterialButton materialButton1;
        private MaterialSkin.Controls.MaterialButton materialButton2;
        private MaterialSkin.Controls.MaterialButton materialButton3;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialTextBox txtID;
    }
}