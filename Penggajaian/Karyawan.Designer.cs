﻿namespace Penggajaian
{
    partial class Karyawan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.materialRadioButton1 = new MaterialSkin.Controls.MaterialRadioButton();
            this.materialRadioButton2 = new MaterialSkin.Controls.MaterialRadioButton();
            this.txtNamaKaryawan = new MaterialSkin.Controls.MaterialTextBox();
            this.txtAlamat = new MaterialSkin.Controls.MaterialTextBox();
            this.txtNoHp = new MaterialSkin.Controls.MaterialTextBox();
            this.txtTempatLahir = new MaterialSkin.Controls.MaterialTextBox();
            this.txtJenisKelamin = new MaterialSkin.Controls.MaterialComboBox();
            this.txtTanggalLahir = new System.Windows.Forms.DateTimePicker();
            this.txtStatus = new MaterialSkin.Controls.MaterialComboBox();
            this.txtJumlahAnak = new MaterialSkin.Controls.MaterialTextBox();
            this.txtUsername = new MaterialSkin.Controls.MaterialTextBox();
            this.txtPassword = new MaterialSkin.Controls.MaterialTextBox();
            this.materialFloatingActionButton1 = new MaterialSkin.Controls.MaterialFloatingActionButton();
            this.btnSimpan = new MaterialSkin.Controls.MaterialButton();
            this.btnUpdate = new MaterialSkin.Controls.MaterialButton();
            this.btnHapus = new MaterialSkin.Controls.MaterialButton();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.txtJabatan = new MaterialSkin.Controls.MaterialComboBox();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.txtID = new MaterialSkin.Controls.MaterialTextBox();
            this.materialListView1 = new MaterialSkin.Controls.MaterialListView();
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nama_karyawan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.alamat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.no_hp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tempat_lahir = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tanggal_lahir = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.jenis_kelamin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.id_jabatan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.jumlah_anak = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.username = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.password = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(84, 354);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(119, 19);
            this.materialLabel1.TabIndex = 0;
            this.materialLabel1.Text = "Nama Karyawan";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(84, 398);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(52, 19);
            this.materialLabel2.TabIndex = 1;
            this.materialLabel2.Text = "Alamat";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(84, 446);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(46, 19);
            this.materialLabel3.TabIndex = 2;
            this.materialLabel3.Text = "No HP";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(82, 487);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(96, 19);
            this.materialLabel4.TabIndex = 3;
            this.materialLabel4.Text = "Tempat Lahir";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(82, 531);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(100, 19);
            this.materialLabel5.TabIndex = 4;
            this.materialLabel5.Text = "Tanggal Lahir";
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(82, 575);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(101, 19);
            this.materialLabel6.TabIndex = 5;
            this.materialLabel6.Text = "Jenis Kelamin";
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(514, 400);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(95, 19);
            this.materialLabel7.TabIndex = 6;
            this.materialLabel7.Text = "Jumlah Anak";
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(514, 355);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(47, 19);
            this.materialLabel8.TabIndex = 7;
            this.materialLabel8.Text = "Status";
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(514, 447);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(72, 19);
            this.materialLabel9.TabIndex = 8;
            this.materialLabel9.Text = "Username";
            // 
            // materialLabel10
            // 
            this.materialLabel10.AutoSize = true;
            this.materialLabel10.Depth = 0;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel10.Location = new System.Drawing.Point(515, 502);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(71, 19);
            this.materialLabel10.TabIndex = 9;
            this.materialLabel10.Text = "Password";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(174, 275);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(174, 20);
            this.dateTimePicker1.TabIndex = 14;
            // 
            // materialRadioButton1
            // 
            this.materialRadioButton1.AutoSize = true;
            this.materialRadioButton1.Depth = 0;
            this.materialRadioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.materialRadioButton1.Location = new System.Drawing.Point(174, 353);
            this.materialRadioButton1.Margin = new System.Windows.Forms.Padding(0);
            this.materialRadioButton1.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialRadioButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRadioButton1.Name = "materialRadioButton1";
            this.materialRadioButton1.Ripple = true;
            this.materialRadioButton1.Size = new System.Drawing.Size(100, 30);
            this.materialRadioButton1.TabIndex = 15;
            this.materialRadioButton1.TabStop = true;
            this.materialRadioButton1.Text = "Perempuan";
            this.materialRadioButton1.UseVisualStyleBackColor = true;
            // 
            // materialRadioButton2
            // 
            this.materialRadioButton2.AutoSize = true;
            this.materialRadioButton2.Depth = 0;
            this.materialRadioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.materialRadioButton2.Location = new System.Drawing.Point(174, 314);
            this.materialRadioButton2.Margin = new System.Windows.Forms.Padding(0);
            this.materialRadioButton2.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialRadioButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRadioButton2.Name = "materialRadioButton2";
            this.materialRadioButton2.Ripple = true;
            this.materialRadioButton2.Size = new System.Drawing.Size(83, 30);
            this.materialRadioButton2.TabIndex = 16;
            this.materialRadioButton2.TabStop = true;
            this.materialRadioButton2.Text = "Laki Laki";
            this.materialRadioButton2.UseVisualStyleBackColor = true;
            // 
            // txtNamaKaryawan
            // 
            this.txtNamaKaryawan.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNamaKaryawan.Depth = 0;
            this.txtNamaKaryawan.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtNamaKaryawan.Location = new System.Drawing.Point(215, 337);
            this.txtNamaKaryawan.MaxLength = 50;
            this.txtNamaKaryawan.MouseState = MaterialSkin.MouseState.OUT;
            this.txtNamaKaryawan.Multiline = false;
            this.txtNamaKaryawan.Name = "txtNamaKaryawan";
            this.txtNamaKaryawan.Size = new System.Drawing.Size(185, 36);
            this.txtNamaKaryawan.TabIndex = 10;
            this.txtNamaKaryawan.Text = "";
            this.txtNamaKaryawan.UseTallSize = false;
            // 
            // txtAlamat
            // 
            this.txtAlamat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAlamat.Depth = 0;
            this.txtAlamat.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtAlamat.Location = new System.Drawing.Point(215, 379);
            this.txtAlamat.MaxLength = 50;
            this.txtAlamat.MouseState = MaterialSkin.MouseState.OUT;
            this.txtAlamat.Multiline = false;
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.Size = new System.Drawing.Size(185, 36);
            this.txtAlamat.TabIndex = 11;
            this.txtAlamat.Text = "";
            this.txtAlamat.UseTallSize = false;
            // 
            // txtNoHp
            // 
            this.txtNoHp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNoHp.Depth = 0;
            this.txtNoHp.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtNoHp.Location = new System.Drawing.Point(215, 429);
            this.txtNoHp.MaxLength = 50;
            this.txtNoHp.MouseState = MaterialSkin.MouseState.OUT;
            this.txtNoHp.Multiline = false;
            this.txtNoHp.Name = "txtNoHp";
            this.txtNoHp.Size = new System.Drawing.Size(185, 36);
            this.txtNoHp.TabIndex = 12;
            this.txtNoHp.Text = "";
            this.txtNoHp.UseTallSize = false;
            // 
            // txtTempatLahir
            // 
            this.txtTempatLahir.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTempatLahir.Depth = 0;
            this.txtTempatLahir.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtTempatLahir.Location = new System.Drawing.Point(215, 484);
            this.txtTempatLahir.MaxLength = 50;
            this.txtTempatLahir.MouseState = MaterialSkin.MouseState.OUT;
            this.txtTempatLahir.Multiline = false;
            this.txtTempatLahir.Name = "txtTempatLahir";
            this.txtTempatLahir.Size = new System.Drawing.Size(185, 36);
            this.txtTempatLahir.TabIndex = 13;
            this.txtTempatLahir.Text = "";
            this.txtTempatLahir.UseTallSize = false;
            // 
            // txtJenisKelamin
            // 
            this.txtJenisKelamin.AutoResize = false;
            this.txtJenisKelamin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtJenisKelamin.Depth = 0;
            this.txtJenisKelamin.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.txtJenisKelamin.DropDownHeight = 118;
            this.txtJenisKelamin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtJenisKelamin.DropDownWidth = 121;
            this.txtJenisKelamin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtJenisKelamin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtJenisKelamin.FormattingEnabled = true;
            this.txtJenisKelamin.IntegralHeight = false;
            this.txtJenisKelamin.ItemHeight = 29;
            this.txtJenisKelamin.Items.AddRange(new object[] {
            "P",
            "L"});
            this.txtJenisKelamin.Location = new System.Drawing.Point(215, 567);
            this.txtJenisKelamin.MaxDropDownItems = 4;
            this.txtJenisKelamin.MouseState = MaterialSkin.MouseState.OUT;
            this.txtJenisKelamin.Name = "txtJenisKelamin";
            this.txtJenisKelamin.Size = new System.Drawing.Size(185, 35);
            this.txtJenisKelamin.TabIndex = 14;
            this.txtJenisKelamin.UseTallSize = false;
            // 
            // txtTanggalLahir
            // 
            this.txtTanggalLahir.Location = new System.Drawing.Point(215, 531);
            this.txtTanggalLahir.Name = "txtTanggalLahir";
            this.txtTanggalLahir.Size = new System.Drawing.Size(185, 20);
            this.txtTanggalLahir.TabIndex = 15;
            // 
            // txtStatus
            // 
            this.txtStatus.AutoResize = false;
            this.txtStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtStatus.Depth = 0;
            this.txtStatus.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.txtStatus.DropDownHeight = 118;
            this.txtStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtStatus.DropDownWidth = 121;
            this.txtStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtStatus.FormattingEnabled = true;
            this.txtStatus.IntegralHeight = false;
            this.txtStatus.ItemHeight = 29;
            this.txtStatus.Items.AddRange(new object[] {
            "Menikah",
            "Belum Menikah"});
            this.txtStatus.Location = new System.Drawing.Point(620, 338);
            this.txtStatus.MaxDropDownItems = 4;
            this.txtStatus.MouseState = MaterialSkin.MouseState.OUT;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(185, 35);
            this.txtStatus.TabIndex = 16;
            this.txtStatus.UseTallSize = false;
            this.txtStatus.SelectedIndexChanged += new System.EventHandler(this.TxtStatus_SelectedIndexChanged);
            // 
            // txtJumlahAnak
            // 
            this.txtJumlahAnak.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtJumlahAnak.Depth = 0;
            this.txtJumlahAnak.Enabled = false;
            this.txtJumlahAnak.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtJumlahAnak.Location = new System.Drawing.Point(620, 397);
            this.txtJumlahAnak.MaxLength = 50;
            this.txtJumlahAnak.MouseState = MaterialSkin.MouseState.OUT;
            this.txtJumlahAnak.Multiline = false;
            this.txtJumlahAnak.Name = "txtJumlahAnak";
            this.txtJumlahAnak.Size = new System.Drawing.Size(185, 36);
            this.txtJumlahAnak.TabIndex = 17;
            this.txtJumlahAnak.Text = "";
            this.txtJumlahAnak.UseTallSize = false;
            // 
            // txtUsername
            // 
            this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUsername.Depth = 0;
            this.txtUsername.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtUsername.Location = new System.Drawing.Point(620, 447);
            this.txtUsername.MaxLength = 50;
            this.txtUsername.MouseState = MaterialSkin.MouseState.OUT;
            this.txtUsername.Multiline = false;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(185, 36);
            this.txtUsername.TabIndex = 18;
            this.txtUsername.Text = "";
            this.txtUsername.UseTallSize = false;
            // 
            // txtPassword
            // 
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.Depth = 0;
            this.txtPassword.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtPassword.Location = new System.Drawing.Point(620, 499);
            this.txtPassword.MaxLength = 50;
            this.txtPassword.MouseState = MaterialSkin.MouseState.OUT;
            this.txtPassword.Multiline = false;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Password = true;
            this.txtPassword.Size = new System.Drawing.Size(185, 36);
            this.txtPassword.TabIndex = 19;
            this.txtPassword.Text = "";
            this.txtPassword.UseTallSize = false;
            // 
            // materialFloatingActionButton1
            // 
            this.materialFloatingActionButton1.AnimateShowHideButton = false;
            this.materialFloatingActionButton1.Depth = 0;
            this.materialFloatingActionButton1.DrawShadows = true;
            this.materialFloatingActionButton1.Icon = global::Penggajaian.Properties.Resources.icons8_arrow_pointing_left_96;
            this.materialFloatingActionButton1.Location = new System.Drawing.Point(844, 544);
            this.materialFloatingActionButton1.Mini = false;
            this.materialFloatingActionButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialFloatingActionButton1.Name = "materialFloatingActionButton1";
            this.materialFloatingActionButton1.Size = new System.Drawing.Size(56, 56);
            this.materialFloatingActionButton1.TabIndex = 0;
            this.materialFloatingActionButton1.Text = "materialFloatingActionButton1";
            this.materialFloatingActionButton1.UseVisualStyleBackColor = true;
            this.materialFloatingActionButton1.Click += new System.EventHandler(this.MaterialFloatingActionButton1_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSimpan.Depth = 0;
            this.btnSimpan.DrawShadows = true;
            this.btnSimpan.HighEmphasis = true;
            this.btnSimpan.Icon = null;
            this.btnSimpan.Location = new System.Drawing.Point(532, 554);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnSimpan.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(77, 36);
            this.btnSimpan.TabIndex = 20;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.btnSimpan.UseAccentColor = false;
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.BtnSimpan_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUpdate.Depth = 0;
            this.btnUpdate.DrawShadows = true;
            this.btnUpdate.HighEmphasis = true;
            this.btnUpdate.Icon = null;
            this.btnUpdate.Location = new System.Drawing.Point(627, 554);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnUpdate.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(77, 36);
            this.btnUpdate.TabIndex = 21;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.btnUpdate.UseAccentColor = false;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
            // 
            // btnHapus
            // 
            this.btnHapus.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnHapus.Depth = 0;
            this.btnHapus.DrawShadows = true;
            this.btnHapus.HighEmphasis = true;
            this.btnHapus.Icon = null;
            this.btnHapus.Location = new System.Drawing.Point(728, 554);
            this.btnHapus.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnHapus.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(69, 36);
            this.btnHapus.TabIndex = 22;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.btnHapus.UseAccentColor = false;
            this.btnHapus.UseVisualStyleBackColor = true;
            this.btnHapus.Click += new System.EventHandler(this.BtnHapus_Click);
            // 
            // materialLabel11
            // 
            this.materialLabel11.AutoSize = true;
            this.materialLabel11.Depth = 0;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.Location = new System.Drawing.Point(515, 298);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(60, 19);
            this.materialLabel11.TabIndex = 24;
            this.materialLabel11.Text = "Jabatan";
            // 
            // txtJabatan
            // 
            this.txtJabatan.AutoResize = false;
            this.txtJabatan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtJabatan.Depth = 0;
            this.txtJabatan.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.txtJabatan.DropDownHeight = 118;
            this.txtJabatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtJabatan.DropDownWidth = 121;
            this.txtJabatan.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtJabatan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtJabatan.FormattingEnabled = true;
            this.txtJabatan.IntegralHeight = false;
            this.txtJabatan.ItemHeight = 29;
            this.txtJabatan.Location = new System.Drawing.Point(620, 294);
            this.txtJabatan.MaxDropDownItems = 4;
            this.txtJabatan.MouseState = MaterialSkin.MouseState.OUT;
            this.txtJabatan.Name = "txtJabatan";
            this.txtJabatan.Size = new System.Drawing.Size(185, 35);
            this.txtJabatan.TabIndex = 25;
            this.txtJabatan.UseTallSize = false;
            // 
            // materialLabel12
            // 
            this.materialLabel12.AutoSize = true;
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.Location = new System.Drawing.Point(84, 298);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(20, 19);
            this.materialLabel12.TabIndex = 26;
            this.materialLabel12.Text = "ID ";
            // 
            // txtID
            // 
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtID.Depth = 0;
            this.txtID.Enabled = false;
            this.txtID.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtID.Location = new System.Drawing.Point(215, 293);
            this.txtID.MaxLength = 50;
            this.txtID.MouseState = MaterialSkin.MouseState.OUT;
            this.txtID.Multiline = false;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(185, 36);
            this.txtID.TabIndex = 27;
            this.txtID.Text = "";
            this.txtID.UseTallSize = false;
            // 
            // materialListView1
            // 
            this.materialListView1.AutoSizeTable = false;
            this.materialListView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.materialListView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.nama_karyawan,
            this.alamat,
            this.no_hp,
            this.tempat_lahir,
            this.tanggal_lahir,
            this.jenis_kelamin,
            this.id_jabatan,
            this.status,
            this.jumlah_anak,
            this.username,
            this.password});
            this.materialListView1.Depth = 0;
            this.materialListView1.FullRowSelect = true;
            this.materialListView1.HideSelection = false;
            this.materialListView1.Location = new System.Drawing.Point(68, 73);
            this.materialListView1.MinimumSize = new System.Drawing.Size(200, 100);
            this.materialListView1.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialListView1.MouseState = MaterialSkin.MouseState.OUT;
            this.materialListView1.Name = "materialListView1";
            this.materialListView1.OwnerDraw = true;
            this.materialListView1.Size = new System.Drawing.Size(775, 197);
            this.materialListView1.TabIndex = 28;
            this.materialListView1.UseCompatibleStateImageBehavior = false;
            this.materialListView1.View = System.Windows.Forms.View.Details;
            this.materialListView1.SelectedIndexChanged += new System.EventHandler(this.MaterialListView1_SelectedIndexChanged_1);
            // 
            // ID
            // 
            this.ID.Text = "ID";
            this.ID.Width = 80;
            // 
            // nama_karyawan
            // 
            this.nama_karyawan.Text = "Nama Karyawan";
            this.nama_karyawan.Width = 140;
            // 
            // alamat
            // 
            this.alamat.Text = "Alamat";
            this.alamat.Width = 120;
            // 
            // no_hp
            // 
            this.no_hp.Text = "No HP";
            this.no_hp.Width = 120;
            // 
            // tempat_lahir
            // 
            this.tempat_lahir.Text = "Tempat Lahir";
            this.tempat_lahir.Width = 120;
            // 
            // tanggal_lahir
            // 
            this.tanggal_lahir.Text = "Tanggal Lahir";
            this.tanggal_lahir.Width = 120;
            // 
            // jenis_kelamin
            // 
            this.jenis_kelamin.Text = "Jenis Kelamin";
            this.jenis_kelamin.Width = 120;
            // 
            // id_jabatan
            // 
            this.id_jabatan.Text = "ID Jabatan";
            this.id_jabatan.Width = 80;
            // 
            // status
            // 
            this.status.Text = "Status";
            this.status.Width = 120;
            // 
            // jumlah_anak
            // 
            this.jumlah_anak.Text = "Jumlah Anak";
            this.jumlah_anak.Width = 120;
            // 
            // username
            // 
            this.username.Text = "Username";
            this.username.Width = 120;
            // 
            // password
            // 
            this.password.Text = "Password";
            this.password.Width = 120;
            // 
            // Karyawan
            // 
            this.AllowDrop = true;
            this.ClientSize = new System.Drawing.Size(914, 618);
            this.ControlBox = false;
            this.Controls.Add(this.materialListView1);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.materialLabel12);
            this.Controls.Add(this.txtJabatan);
            this.Controls.Add(this.materialLabel11);
            this.Controls.Add(this.btnHapus);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.materialFloatingActionButton1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.txtJumlahAnak);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.txtTanggalLahir);
            this.Controls.Add(this.txtJenisKelamin);
            this.Controls.Add(this.txtTempatLahir);
            this.Controls.Add(this.txtNoHp);
            this.Controls.Add(this.txtAlamat);
            this.Controls.Add(this.txtNamaKaryawan);
            this.Controls.Add(this.materialLabel10);
            this.Controls.Add(this.materialLabel9);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.materialLabel7);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.materialLabel1);
            this.Name = "Karyawan";
            this.Text = "Karyawan";
            this.Load += new System.EventHandler(this.Karyawan_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButton1;
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButton2;
        private MaterialSkin.Controls.MaterialTextBox txtNamaKaryawan;
        private MaterialSkin.Controls.MaterialTextBox txtAlamat;
        private MaterialSkin.Controls.MaterialTextBox txtNoHp;
        private MaterialSkin.Controls.MaterialTextBox txtTempatLahir;
        private MaterialSkin.Controls.MaterialComboBox txtJenisKelamin;
        private System.Windows.Forms.DateTimePicker txtTanggalLahir;
        private MaterialSkin.Controls.MaterialComboBox txtStatus;
        private MaterialSkin.Controls.MaterialTextBox txtJumlahAnak;
        private MaterialSkin.Controls.MaterialTextBox txtUsername;
        private MaterialSkin.Controls.MaterialTextBox txtPassword;
        private MaterialSkin.Controls.MaterialFloatingActionButton materialFloatingActionButton1;
        private MaterialSkin.Controls.MaterialButton btnSimpan;
        private MaterialSkin.Controls.MaterialButton btnUpdate;
        private MaterialSkin.Controls.MaterialButton btnHapus;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialComboBox txtJabatan;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private MaterialSkin.Controls.MaterialTextBox txtID;
        private MaterialSkin.Controls.MaterialListView materialListView1;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader nama_karyawan;
        private System.Windows.Forms.ColumnHeader alamat;
        private System.Windows.Forms.ColumnHeader no_hp;
        private System.Windows.Forms.ColumnHeader tempat_lahir;
        private System.Windows.Forms.ColumnHeader tanggal_lahir;
        private System.Windows.Forms.ColumnHeader jenis_kelamin;
        private System.Windows.Forms.ColumnHeader id_jabatan;
        private System.Windows.Forms.ColumnHeader status;
        private System.Windows.Forms.ColumnHeader jumlah_anak;
        private System.Windows.Forms.ColumnHeader username;
        private System.Windows.Forms.ColumnHeader password;
    }
}