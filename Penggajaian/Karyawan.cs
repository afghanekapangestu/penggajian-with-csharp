﻿using MaterialSkin;
using MaterialSkin.Controls;
using MySql.Data.MySqlClient;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace Penggajaian
{
    public partial class Karyawan : MaterialForm

    {
        string database = "server = localhost; database=aplikasi_karyawan;uid=root; pwd=''";
        public MySqlConnection koneksi;
        public MySqlCommand command;

        public MySqlDataAdapter adp;
        public MySqlDataReader sdr;

        public Karyawan()
        {
            InitializeComponent();
            Sizable = false;
            bacaJabatan();
            bacaData();




            // Create a material theme manager and add the form to manage (this)
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;

            // Configure color schema
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue400, Primary.Blue500,
                Primary.Blue500, Accent.LightBlue200,
                TextShade.WHITE
            );
        }

        private void MaterialFloatingActionButton1_Click(object sender, EventArgs e)
        {
            Menu mn = new Menu();
            this.Hide();
            mn.ShowDialog();
            this.Close();
        }

        private void Karyawan_Load(object sender, EventArgs e)
        {
        }

        private void bacaJabatan()
        {
            //MySqlConnection mc = new MySqlConnection(database);
            //koneksi.Open();
            //command = new MySqlCommand("SELECT * FROM jabatan",koneksi);
            //sdr = command.ExecuteReader();

            //while (sdr.Read())
            //{
            //    for(int i = 0; i<sdr.FieldCount; i++)
            //    {
            //        txtJabatan.Items.Add(sdr.GetString(i));
            //    }
            //    sdr.Close();
            //    koneksi.Close();
            //}

            string sql = "SELECT nama_jabatan FROM jabatan";
            koneksi = new MySqlConnection(database);
            koneksi.Open();
            command = new MySqlCommand(sql, koneksi);
            sdr = command.ExecuteReader();

            while (sdr.Read())
            {
                txtJabatan.Items.Add(sdr["nama_jabatan"].ToString());
            }

        }


        private void bacaData()
        {
            string sql = "SELECT * FROM karyawan";
            koneksi = new MySqlConnection(database);
            koneksi.Open();
            command = new MySqlCommand(sql, koneksi);
            sdr = command.ExecuteReader();

            while (sdr.Read())
            {
                string[] row = { sdr["id"].ToString(), sdr["nama_karyawan"].ToString(), sdr["alamat"].ToString(), sdr["no_hp"].ToString(), sdr["tempat_lahir"].ToString(), sdr["tanggal_lahir"].ToString(), sdr["jenis_kelamin"].ToString(), sdr["id_jabatan"].ToString(), sdr["status"].ToString(), sdr["jml_anak"].ToString(), sdr["username"].ToString(), sdr["password"].ToString() };
                var listviewitem = new ListViewItem(row);
                materialListView1.Items.Add(listviewitem);
            }

        }



        

       

        private void TxtStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtStatus.Text == "Belum Menikah")
            {
                txtJumlahAnak.Enabled = false;
                txtJumlahAnak.Text = "0";
            }
            else if (txtStatus.Text == "Menikah")
            {
                txtJumlahAnak.Enabled = true;

            }
        }

        private void UseClear()
        {
            foreach (Control control in this.Controls)
            {
                if (control is MaterialTextBox)
                {
                    MaterialTextBox textbox = control as MaterialTextBox;
                    textbox.Clear();
                }
            }

            foreach (Control control in this.Controls)
            {
                if (control is MaterialComboBox)
                {
                    MaterialComboBox combobox = control as MaterialComboBox;
                    combobox.SelectedItem = null;
                }
            }

        }

        public Boolean Query(string query)
        {
            koneksi = new MySqlConnection(database);
            try
            {
                koneksi.Open();
                command = new MySqlCommand(query, koneksi);
                command.ExecuteNonQuery();
                return true;
            }catch(Exception ewin)
            {
                MessageBox.Show(ewin.Message);
            }
            finally
            {
                koneksi.Close();
            }
            return false;
        }

       

        private void MaterialListView1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (materialListView1.SelectedItems.Count > 0)
            {
                ListViewItem item = materialListView1.SelectedItems[0];
                txtID.Text = item.SubItems[0].Text;
                txtNamaKaryawan.Text = item.SubItems[1].Text;
                txtAlamat.Text = item.SubItems[2].Text;
                txtNoHp.Text = item.SubItems[3].Text;
                txtTempatLahir.Text = item.SubItems[4].Text;
                String date_data = item.SubItems[5].Text;
                DateTime time = DateTime.ParseExact(date_data, "yyyy-MM-dd", null);
                txtTanggalLahir.Value = time;

                //txtTanggalLahir.Text = item.SubItems[5].Text;
                txtJenisKelamin.Text = item.SubItems[6].Text;

                string bacajabatan = "SELECT nama_jabatan FROM jabatan WHERE id='"+item.SubItems[7].Text+"'";

                koneksi = new MySqlConnection(database);
                koneksi.Open();
                command = new MySqlCommand(bacajabatan, koneksi);
                sdr = command.ExecuteReader();

                while (sdr.Read())
                {
                    txtJabatan.Text = sdr["nama_jabatan"].ToString();
                }

                //if(item.SubItems[7].Text == "1")
                //{
                //    txtJabatan.Text = "Kepsek";
                //}else if(item.SubItems[7].Text == "2")
                //{
                //    txtJabatan.Text = "Waka";
                //}
                //else if (item.SubItems[7].Text == "3")
                //{
                //    txtJabatan.Text = "Kaprog";
                //}
                //else if (item.SubItems[7].Text == "4")
                //{
                //    txtJabatan.Text = "Guru";
                //}
                //else if (item.SubItems[7].Text == "5")
                //{
                //    txtJabatan.Text = "Tata Usaha";
                //}
                //else if (item.SubItems[7].Text == "6")
                //{
                //    txtJabatan.Text = "Office Boy";
                //}

                txtStatus.Text = item.SubItems[8].Text;
                txtJumlahAnak.Text = item.SubItems[9].Text;
                txtUsername.Text = item.SubItems[10].Text;
                txtPassword.Text = item.SubItems[11].Text;


            }
            else
            {
                UseClear();
            }
        }

        private void BtnSimpan_Click(object sender, EventArgs e)
        {
            try
            {
                string Date = txtTanggalLahir.Value.Date.ToString("yyyy-MM-dd");

                int id_jabatan = 0;

                string query = "SELECT id FROM `jabatan` WHERE nama_jabatan = '"+txtJabatan.Text+"'";

                koneksi = new MySqlConnection(database);
                koneksi.Open();
                command = new MySqlCommand(query, koneksi);
                sdr = command.ExecuteReader();

                while (sdr.Read())
                {
                    id_jabatan = sdr.GetInt32(0);
                }




                string Query = "INSERT INTO karyawan VALUES('','" + txtNamaKaryawan.Text + "','" + txtAlamat.Text + "','" + txtNoHp.Text + "','" + txtTempatLahir.Text + "','" + Date + "','" + txtJenisKelamin.Text + "','" + id_jabatan + "','" + txtStatus.Text + "','" + txtJumlahAnak.Text + "','" + txtUsername.Text + "','" + txtPassword.Text + "')";

                koneksi = new MySqlConnection(database);
                command = new MySqlCommand(Query, koneksi);

                koneksi.Open();
                sdr = command.ExecuteReader();
                materialListView1.Items.Clear();
                bacaData();
                UseClear();
                MessageBox.Show("Data Berhasil di Simpan");
            }
            catch (Exception ali)
            {
                MessageBox.Show("Error" + ali.Message);
            }
        }

        private void BtnHapus_Click(object sender, EventArgs e)
        {

            delete();

            UseClear();
        }

        private void delete()
        {
            string query = "DELETE FROM karyawan WHERE id='" + txtID.Text+"'" ;


            
            
            if (this.Query(query))
            {
                MessageBox.Show("Data Berhasil Dihapus");
                materialListView1.Items.Clear();
                bacaData();
                UseClear();
            }
            else
            {
                MessageBox.Show("Data Tidak Berhasil Dihapus");
            }
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            int id_jabatan = 0;

            string sql = "SELECT id FROM `jabatan` WHERE nama_jabatan = '" + txtJabatan.Text + "'";

            koneksi = new MySqlConnection(database);
            koneksi.Open();
            command = new MySqlCommand(sql, koneksi);
            sdr = command.ExecuteReader();

            while (sdr.Read())
            {
                id_jabatan = sdr.GetInt32(0);
            }
            string Date = txtTanggalLahir.Value.Date.ToString("yyyy-MM-dd");
            string query = "UPDATE karyawan SET nama_karyawan='"+txtNamaKaryawan.Text+ "',alamat='" + txtAlamat.Text + "',no_hp='" + txtNoHp.Text + "',tempat_lahir='" + txtTempatLahir.Text + "',tanggal_lahir='" + Date + "',jenis_kelamin='" + txtJenisKelamin.Text + "',id_jabatan='" + id_jabatan + "',status='" + txtStatus.Text + "',jml_anak='" + txtJumlahAnak.Text + "',username='" + txtUsername.Text+ "',password='" + txtPassword.Text+ "' WHERE id='"+txtID.Text+"'";

            if (this.Query(query))
            {
                MessageBox.Show("Data Berhasil diupdate");
                materialListView1.Items.Clear();
                bacaData();
                UseClear();
            }
            else
            {
                MessageBox.Show("Data Tidak Berhasil diupdate");
            }

        }
    }
}

