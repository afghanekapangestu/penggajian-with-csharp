﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;

namespace Penggajaian
{
    public partial class ReportGajiKaryawan : Form
    {
        public ReportGajiKaryawan()
        {
            InitializeComponent();
        }

        private void ReportGajiKaryawan_Load(object sender, EventArgs e)
        {
            DataGaji s = new DataGaji();
            string koneksi = "server=localhost;uid=root;database=aplikasi_karyawan;pwd = ''";
            string sql = "SELECT * FROM gaji";
            MySqlConnection cn = new MySqlConnection(koneksi);
            MySqlDataAdapter da = new MySqlDataAdapter(sql, koneksi);
            da.Fill(s, s.Tables[0].TableName);
            ReportDataSource rds = new ReportDataSource("DataGaji", s.Tables[0]);

            reportViewer1.LocalReport.ReportPath = "../../ReportGaji.rdlc";

            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();


        }
    }
}
