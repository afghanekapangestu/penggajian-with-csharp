﻿namespace Penggajaian
{
    partial class Gaji
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.txtIDKaryawan = new MaterialSkin.Controls.MaterialTextBox();
            this.txtGajiKotor = new MaterialSkin.Controls.MaterialTextBox();
            this.txtJumlahLembur = new MaterialSkin.Controls.MaterialTextBox();
            this.txtPajak = new MaterialSkin.Controls.MaterialTextBox();
            this.txtTotalTunjangan = new MaterialSkin.Controls.MaterialTextBox();
            this.txtGajiLembur = new MaterialSkin.Controls.MaterialTextBox();
            this.txtBulan = new System.Windows.Forms.DateTimePicker();
            this.txtTahun = new System.Windows.Forms.DateTimePicker();
            this.materialButton1 = new MaterialSkin.Controls.MaterialButton();
            this.materialButton2 = new MaterialSkin.Controls.MaterialButton();
            this.materialButton3 = new MaterialSkin.Controls.MaterialButton();
            this.materialFloatingActionButton1 = new MaterialSkin.Controls.MaterialFloatingActionButton();
            this.txtSearch = new MaterialSkin.Controls.MaterialTextBox();
            this.materialFloatingActionButton2 = new MaterialSkin.Controls.MaterialFloatingActionButton();
            this.materialListView1 = new MaterialSkin.Controls.MaterialListView();
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nama_karyawan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.alamat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.no_hp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tempat_lahir = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tanggal_lahir = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.jenis_kelamin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.id_jabatan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.jumlah_anak = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.username = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.password = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nama_jabatan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.jumlah_gaji_pokok = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.upah_lembur = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.materialListView2 = new MaterialSkin.Controls.MaterialListView();
            this.idjab = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.id_karyawan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gaji_kotor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.jumlah_lembur = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pajak = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.total_tunjangan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gaji_bersih = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.bulan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tahun = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.txtNamaKaryawan = new MaterialSkin.Controls.MaterialTextBox();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.txtJmlhAnak = new MaterialSkin.Controls.MaterialTextBox();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.txtGajiPokok = new MaterialSkin.Controls.MaterialTextBox();
            this.txtUpahLembur = new MaterialSkin.Controls.MaterialTextBox();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            this.txtGajiBersih = new MaterialSkin.Controls.MaterialTextBox();
            this.textBoxContextMenuStrip1 = new MaterialSkin.Controls.TextBoxContextMenuStrip();
            this.SuspendLayout();
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel2.Location = new System.Drawing.Point(38, 399);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(91, 19);
            this.materialLabel2.TabIndex = 1;
            this.materialLabel2.Text = "ID Karyawan";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel3.Location = new System.Drawing.Point(441, 435);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(71, 19);
            this.materialLabel3.TabIndex = 2;
            this.materialLabel3.Text = "Gaji Kotor";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel4.Location = new System.Drawing.Point(441, 483);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(113, 19);
            this.materialLabel4.TabIndex = 3;
            this.materialLabel4.Text = "Jumlah Lembur";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel5.Location = new System.Drawing.Point(441, 384);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(41, 19);
            this.materialLabel5.TabIndex = 4;
            this.materialLabel5.Text = "Pajak";
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel6.Location = new System.Drawing.Point(32, 651);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(119, 19);
            this.materialLabel6.TabIndex = 5;
            this.materialLabel6.Text = "Total Tunjangan";
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel7.Location = new System.Drawing.Point(442, 521);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(87, 19);
            this.materialLabel7.TabIndex = 6;
            this.materialLabel7.Text = "Gaji Lembur";
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel8.Location = new System.Drawing.Point(442, 604);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(42, 19);
            this.materialLabel8.TabIndex = 7;
            this.materialLabel8.Text = "Bulan";
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel9.Location = new System.Drawing.Point(441, 640);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(47, 19);
            this.materialLabel9.TabIndex = 8;
            this.materialLabel9.Text = "Tahun";
            // 
            // txtIDKaryawan
            // 
            this.txtIDKaryawan.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtIDKaryawan.Depth = 0;
            this.txtIDKaryawan.Enabled = false;
            this.txtIDKaryawan.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtIDKaryawan.Location = new System.Drawing.Point(169, 382);
            this.txtIDKaryawan.MaxLength = 50;
            this.txtIDKaryawan.MouseState = MaterialSkin.MouseState.OUT;
            this.txtIDKaryawan.Multiline = false;
            this.txtIDKaryawan.Name = "txtIDKaryawan";
            this.txtIDKaryawan.Size = new System.Drawing.Size(127, 36);
            this.txtIDKaryawan.TabIndex = 29;
            this.txtIDKaryawan.Text = "";
            this.txtIDKaryawan.UseTallSize = false;
            this.txtIDKaryawan.TextChanged += new System.EventHandler(this.materialTextBox1_TextChanged);
            // 
            // txtGajiKotor
            // 
            this.txtGajiKotor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGajiKotor.Depth = 0;
            this.txtGajiKotor.Enabled = false;
            this.txtGajiKotor.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtGajiKotor.Location = new System.Drawing.Point(572, 418);
            this.txtGajiKotor.MaxLength = 50;
            this.txtGajiKotor.MouseState = MaterialSkin.MouseState.OUT;
            this.txtGajiKotor.Multiline = false;
            this.txtGajiKotor.Name = "txtGajiKotor";
            this.txtGajiKotor.Size = new System.Drawing.Size(127, 36);
            this.txtGajiKotor.TabIndex = 30;
            this.txtGajiKotor.Text = "";
            this.txtGajiKotor.UseTallSize = false;
            // 
            // txtJumlahLembur
            // 
            this.txtJumlahLembur.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtJumlahLembur.Depth = 0;
            this.txtJumlahLembur.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtJumlahLembur.Location = new System.Drawing.Point(572, 466);
            this.txtJumlahLembur.MaxLength = 50;
            this.txtJumlahLembur.MouseState = MaterialSkin.MouseState.OUT;
            this.txtJumlahLembur.Multiline = false;
            this.txtJumlahLembur.Name = "txtJumlahLembur";
            this.txtJumlahLembur.Size = new System.Drawing.Size(127, 36);
            this.txtJumlahLembur.TabIndex = 31;
            this.txtJumlahLembur.Text = "";
            this.txtJumlahLembur.UseTallSize = false;
            this.txtJumlahLembur.TextChanged += new System.EventHandler(this.txtJumlahLembur_TextChanged);
            this.txtJumlahLembur.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtJumlahLembur_KeyDown);
            // 
            // txtPajak
            // 
            this.txtPajak.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPajak.Depth = 0;
            this.txtPajak.Enabled = false;
            this.txtPajak.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtPajak.Location = new System.Drawing.Point(572, 375);
            this.txtPajak.MaxLength = 50;
            this.txtPajak.MouseState = MaterialSkin.MouseState.OUT;
            this.txtPajak.Multiline = false;
            this.txtPajak.Name = "txtPajak";
            this.txtPajak.Size = new System.Drawing.Size(127, 36);
            this.txtPajak.TabIndex = 32;
            this.txtPajak.Text = "";
            this.txtPajak.UseTallSize = false;
            // 
            // txtTotalTunjangan
            // 
            this.txtTotalTunjangan.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotalTunjangan.Depth = 0;
            this.txtTotalTunjangan.Enabled = false;
            this.txtTotalTunjangan.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtTotalTunjangan.Location = new System.Drawing.Point(169, 649);
            this.txtTotalTunjangan.MaxLength = 50;
            this.txtTotalTunjangan.MouseState = MaterialSkin.MouseState.OUT;
            this.txtTotalTunjangan.Multiline = false;
            this.txtTotalTunjangan.Name = "txtTotalTunjangan";
            this.txtTotalTunjangan.Size = new System.Drawing.Size(127, 36);
            this.txtTotalTunjangan.TabIndex = 33;
            this.txtTotalTunjangan.Text = "";
            this.txtTotalTunjangan.UseTallSize = false;
            this.txtTotalTunjangan.TextChanged += new System.EventHandler(this.TxtTotalTunjangan_TextChanged);
            // 
            // txtGajiLembur
            // 
            this.txtGajiLembur.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGajiLembur.Depth = 0;
            this.txtGajiLembur.Enabled = false;
            this.txtGajiLembur.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtGajiLembur.Location = new System.Drawing.Point(572, 512);
            this.txtGajiLembur.MaxLength = 50;
            this.txtGajiLembur.MouseState = MaterialSkin.MouseState.OUT;
            this.txtGajiLembur.Multiline = false;
            this.txtGajiLembur.Name = "txtGajiLembur";
            this.txtGajiLembur.Size = new System.Drawing.Size(127, 36);
            this.txtGajiLembur.TabIndex = 34;
            this.txtGajiLembur.Text = "";
            this.txtGajiLembur.UseTallSize = false;
            // 
            // txtBulan
            // 
            this.txtBulan.Location = new System.Drawing.Point(572, 600);
            this.txtBulan.Name = "txtBulan";
            this.txtBulan.Size = new System.Drawing.Size(127, 20);
            this.txtBulan.TabIndex = 37;
            this.txtBulan.ValueChanged += new System.EventHandler(this.TxtBulan_ValueChanged);
            // 
            // txtTahun
            // 
            this.txtTahun.Location = new System.Drawing.Point(572, 640);
            this.txtTahun.Name = "txtTahun";
            this.txtTahun.Size = new System.Drawing.Size(127, 20);
            this.txtTahun.TabIndex = 38;
            // 
            // materialButton1
            // 
            this.materialButton1.AutoSize = false;
            this.materialButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton1.Depth = 0;
            this.materialButton1.DrawShadows = true;
            this.materialButton1.HighEmphasis = true;
            this.materialButton1.Icon = null;
            this.materialButton1.Location = new System.Drawing.Point(832, 367);
            this.materialButton1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton1.Name = "materialButton1";
            this.materialButton1.Size = new System.Drawing.Size(200, 36);
            this.materialButton1.TabIndex = 40;
            this.materialButton1.Text = "Simpan";
            this.materialButton1.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton1.UseAccentColor = false;
            this.materialButton1.UseVisualStyleBackColor = true;
            this.materialButton1.Click += new System.EventHandler(this.materialButton1_Click);
            // 
            // materialButton2
            // 
            this.materialButton2.AutoSize = false;
            this.materialButton2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton2.Depth = 0;
            this.materialButton2.DrawShadows = true;
            this.materialButton2.HighEmphasis = true;
            this.materialButton2.Icon = null;
            this.materialButton2.Location = new System.Drawing.Point(832, 435);
            this.materialButton2.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton2.Name = "materialButton2";
            this.materialButton2.Size = new System.Drawing.Size(200, 36);
            this.materialButton2.TabIndex = 41;
            this.materialButton2.Text = "Update";
            this.materialButton2.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton2.UseAccentColor = false;
            this.materialButton2.UseVisualStyleBackColor = true;
            this.materialButton2.Click += new System.EventHandler(this.materialButton2_Click);
            // 
            // materialButton3
            // 
            this.materialButton3.AutoSize = false;
            this.materialButton3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialButton3.Depth = 0;
            this.materialButton3.DrawShadows = true;
            this.materialButton3.HighEmphasis = true;
            this.materialButton3.Icon = null;
            this.materialButton3.Location = new System.Drawing.Point(832, 504);
            this.materialButton3.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.materialButton3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialButton3.Name = "materialButton3";
            this.materialButton3.Size = new System.Drawing.Size(200, 36);
            this.materialButton3.TabIndex = 42;
            this.materialButton3.Text = "Hapus";
            this.materialButton3.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.materialButton3.UseAccentColor = false;
            this.materialButton3.UseVisualStyleBackColor = true;
            this.materialButton3.Click += new System.EventHandler(this.materialButton3_Click);
            // 
            // materialFloatingActionButton1
            // 
            this.materialFloatingActionButton1.AnimateShowHideButton = false;
            this.materialFloatingActionButton1.Depth = 0;
            this.materialFloatingActionButton1.DrawShadows = true;
            this.materialFloatingActionButton1.Icon = global::Penggajaian.Properties.Resources.icons8_arrow_pointing_left_96;
            this.materialFloatingActionButton1.Location = new System.Drawing.Point(1045, 690);
            this.materialFloatingActionButton1.Mini = false;
            this.materialFloatingActionButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialFloatingActionButton1.Name = "materialFloatingActionButton1";
            this.materialFloatingActionButton1.Size = new System.Drawing.Size(56, 56);
            this.materialFloatingActionButton1.TabIndex = 43;
            this.materialFloatingActionButton1.Text = "materialFloatingActionButton1";
            this.materialFloatingActionButton1.UseVisualStyleBackColor = true;
            this.materialFloatingActionButton1.Click += new System.EventHandler(this.materialFloatingActionButton1_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSearch.Depth = 0;
            this.txtSearch.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtSearch.Hint = "Search";
            this.txtSearch.Location = new System.Drawing.Point(35, 308);
            this.txtSearch.MaxLength = 50;
            this.txtSearch.MouseState = MaterialSkin.MouseState.OUT;
            this.txtSearch.Multiline = false;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(261, 50);
            this.txtSearch.TabIndex = 45;
            this.txtSearch.Text = "";
            // 
            // materialFloatingActionButton2
            // 
            this.materialFloatingActionButton2.AnimateShowHideButton = true;
            this.materialFloatingActionButton2.Depth = 0;
            this.materialFloatingActionButton2.DrawShadows = true;
            this.materialFloatingActionButton2.Icon = global::Penggajaian.Properties.Resources.icons8_search_24;
            this.materialFloatingActionButton2.Location = new System.Drawing.Point(309, 311);
            this.materialFloatingActionButton2.Mini = true;
            this.materialFloatingActionButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialFloatingActionButton2.Name = "materialFloatingActionButton2";
            this.materialFloatingActionButton2.Size = new System.Drawing.Size(40, 40);
            this.materialFloatingActionButton2.TabIndex = 46;
            this.materialFloatingActionButton2.Text = "materialFloatingActionButton2";
            this.materialFloatingActionButton2.UseVisualStyleBackColor = true;
            this.materialFloatingActionButton2.Click += new System.EventHandler(this.MaterialFloatingActionButton2_Click);
            // 
            // materialListView1
            // 
            this.materialListView1.AutoSizeTable = false;
            this.materialListView1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.materialListView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.nama_karyawan,
            this.alamat,
            this.no_hp,
            this.tempat_lahir,
            this.tanggal_lahir,
            this.jenis_kelamin,
            this.id_jabatan,
            this.status,
            this.jumlah_anak,
            this.username,
            this.password,
            this.nama_jabatan,
            this.jumlah_gaji_pokok,
            this.upah_lembur});
            this.materialListView1.Depth = 0;
            this.materialListView1.FullRowSelect = true;
            this.materialListView1.HideSelection = false;
            this.materialListView1.Location = new System.Drawing.Point(12, 84);
            this.materialListView1.MinimumSize = new System.Drawing.Size(200, 100);
            this.materialListView1.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialListView1.MouseState = MaterialSkin.MouseState.OUT;
            this.materialListView1.Name = "materialListView1";
            this.materialListView1.OwnerDraw = true;
            this.materialListView1.Size = new System.Drawing.Size(574, 197);
            this.materialListView1.TabIndex = 47;
            this.materialListView1.UseCompatibleStateImageBehavior = false;
            this.materialListView1.View = System.Windows.Forms.View.Details;
            this.materialListView1.SelectedIndexChanged += new System.EventHandler(this.materialListView1_SelectedIndexChanged_2);
            // 
            // ID
            // 
            this.ID.Text = "ID Karyawan";
            this.ID.Width = 120;
            // 
            // nama_karyawan
            // 
            this.nama_karyawan.Text = "Nama Karyawan";
            this.nama_karyawan.Width = 140;
            // 
            // alamat
            // 
            this.alamat.Text = "Alamat";
            this.alamat.Width = 120;
            // 
            // no_hp
            // 
            this.no_hp.Text = "No HP";
            this.no_hp.Width = 120;
            // 
            // tempat_lahir
            // 
            this.tempat_lahir.Text = "Tempat Lahir";
            this.tempat_lahir.Width = 120;
            // 
            // tanggal_lahir
            // 
            this.tanggal_lahir.Text = "Tanggal Lahir";
            this.tanggal_lahir.Width = 120;
            // 
            // jenis_kelamin
            // 
            this.jenis_kelamin.Text = "Jenis Kelamin";
            this.jenis_kelamin.Width = 120;
            // 
            // id_jabatan
            // 
            this.id_jabatan.Text = "ID Jabatan";
            this.id_jabatan.Width = 80;
            // 
            // status
            // 
            this.status.Text = "Status";
            this.status.Width = 120;
            // 
            // jumlah_anak
            // 
            this.jumlah_anak.Text = "Jumlah Anak";
            this.jumlah_anak.Width = 120;
            // 
            // username
            // 
            this.username.Text = "Username";
            this.username.Width = 120;
            // 
            // password
            // 
            this.password.Text = "Password";
            this.password.Width = 120;
            // 
            // nama_jabatan
            // 
            this.nama_jabatan.Text = "Nama Jabatan";
            this.nama_jabatan.Width = 120;
            // 
            // jumlah_gaji_pokok
            // 
            this.jumlah_gaji_pokok.Text = "Jumlah Gaji Pokok";
            this.jumlah_gaji_pokok.Width = 120;
            // 
            // upah_lembur
            // 
            this.upah_lembur.Text = "Upah Lembur";
            this.upah_lembur.Width = 120;
            // 
            // materialListView2
            // 
            this.materialListView2.AutoSizeTable = false;
            this.materialListView2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.materialListView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.materialListView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idjab,
            this.id_karyawan,
            this.gaji_kotor,
            this.jumlah_lembur,
            this.pajak,
            this.total_tunjangan,
            this.gaji_bersih,
            this.bulan,
            this.tahun});
            this.materialListView2.Depth = 0;
            this.materialListView2.FullRowSelect = true;
            this.materialListView2.HideSelection = false;
            this.materialListView2.Location = new System.Drawing.Point(604, 84);
            this.materialListView2.MinimumSize = new System.Drawing.Size(200, 100);
            this.materialListView2.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialListView2.MouseState = MaterialSkin.MouseState.OUT;
            this.materialListView2.Name = "materialListView2";
            this.materialListView2.OwnerDraw = true;
            this.materialListView2.Size = new System.Drawing.Size(524, 197);
            this.materialListView2.TabIndex = 48;
            this.materialListView2.UseCompatibleStateImageBehavior = false;
            this.materialListView2.View = System.Windows.Forms.View.Details;
            this.materialListView2.SelectedIndexChanged += new System.EventHandler(this.materialListView2_SelectedIndexChanged);
            // 
            // idjab
            // 
            this.idjab.Text = "ID Jabatan";
            this.idjab.Width = 120;
            // 
            // id_karyawan
            // 
            this.id_karyawan.Text = "ID Karyawan";
            this.id_karyawan.Width = 120;
            // 
            // gaji_kotor
            // 
            this.gaji_kotor.Text = "Gaji Kotor";
            this.gaji_kotor.Width = 125;
            // 
            // jumlah_lembur
            // 
            this.jumlah_lembur.Text = "Jumlah Lembur";
            this.jumlah_lembur.Width = 140;
            // 
            // pajak
            // 
            this.pajak.Text = "Pajak";
            this.pajak.Width = 120;
            // 
            // total_tunjangan
            // 
            this.total_tunjangan.Text = "Total Tunjangan";
            this.total_tunjangan.Width = 150;
            // 
            // gaji_bersih
            // 
            this.gaji_bersih.Text = "Gaji Bersih";
            // 
            // bulan
            // 
            this.bulan.Text = "Bulan";
            this.bulan.Width = 120;
            // 
            // tahun
            // 
            this.tahun.Text = "Tahun";
            this.tahun.Width = 120;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel1.Location = new System.Drawing.Point(38, 452);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(119, 19);
            this.materialLabel1.TabIndex = 49;
            this.materialLabel1.Text = "Nama Karyawan";
            // 
            // txtNamaKaryawan
            // 
            this.txtNamaKaryawan.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNamaKaryawan.Depth = 0;
            this.txtNamaKaryawan.Enabled = false;
            this.txtNamaKaryawan.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtNamaKaryawan.Location = new System.Drawing.Point(169, 435);
            this.txtNamaKaryawan.MaxLength = 50;
            this.txtNamaKaryawan.MouseState = MaterialSkin.MouseState.OUT;
            this.txtNamaKaryawan.Multiline = false;
            this.txtNamaKaryawan.Name = "txtNamaKaryawan";
            this.txtNamaKaryawan.Size = new System.Drawing.Size(127, 36);
            this.txtNamaKaryawan.TabIndex = 50;
            this.txtNamaKaryawan.Text = "";
            this.txtNamaKaryawan.UseTallSize = false;
            // 
            // materialLabel10
            // 
            this.materialLabel10.AutoSize = true;
            this.materialLabel10.Depth = 0;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel10.Location = new System.Drawing.Point(38, 500);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(95, 19);
            this.materialLabel10.TabIndex = 51;
            this.materialLabel10.Text = "Jumlah Anak";
            // 
            // txtJmlhAnak
            // 
            this.txtJmlhAnak.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtJmlhAnak.Depth = 0;
            this.txtJmlhAnak.Enabled = false;
            this.txtJmlhAnak.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtJmlhAnak.Location = new System.Drawing.Point(169, 483);
            this.txtJmlhAnak.MaxLength = 50;
            this.txtJmlhAnak.MouseState = MaterialSkin.MouseState.OUT;
            this.txtJmlhAnak.Multiline = false;
            this.txtJmlhAnak.Name = "txtJmlhAnak";
            this.txtJmlhAnak.Size = new System.Drawing.Size(127, 36);
            this.txtJmlhAnak.TabIndex = 52;
            this.txtJmlhAnak.Text = "";
            this.txtJmlhAnak.UseTallSize = false;
            // 
            // materialLabel11
            // 
            this.materialLabel11.AutoSize = true;
            this.materialLabel11.Depth = 0;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel11.Location = new System.Drawing.Point(38, 546);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(77, 19);
            this.materialLabel11.TabIndex = 53;
            this.materialLabel11.Text = "Gaji Pokok";
            // 
            // txtGajiPokok
            // 
            this.txtGajiPokok.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGajiPokok.Depth = 0;
            this.txtGajiPokok.Enabled = false;
            this.txtGajiPokok.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtGajiPokok.Location = new System.Drawing.Point(169, 529);
            this.txtGajiPokok.MaxLength = 50;
            this.txtGajiPokok.MouseState = MaterialSkin.MouseState.OUT;
            this.txtGajiPokok.Multiline = false;
            this.txtGajiPokok.Name = "txtGajiPokok";
            this.txtGajiPokok.Size = new System.Drawing.Size(127, 36);
            this.txtGajiPokok.TabIndex = 54;
            this.txtGajiPokok.Text = "";
            this.txtGajiPokok.UseTallSize = false;
            // 
            // txtUpahLembur
            // 
            this.txtUpahLembur.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUpahLembur.Depth = 0;
            this.txtUpahLembur.Enabled = false;
            this.txtUpahLembur.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtUpahLembur.Location = new System.Drawing.Point(169, 587);
            this.txtUpahLembur.MaxLength = 50;
            this.txtUpahLembur.MouseState = MaterialSkin.MouseState.OUT;
            this.txtUpahLembur.Multiline = false;
            this.txtUpahLembur.Name = "txtUpahLembur";
            this.txtUpahLembur.Size = new System.Drawing.Size(127, 36);
            this.txtUpahLembur.TabIndex = 55;
            this.txtUpahLembur.Text = "";
            this.txtUpahLembur.UseTallSize = false;
            // 
            // materialLabel12
            // 
            this.materialLabel12.AutoSize = true;
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel12.Location = new System.Drawing.Point(38, 604);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(96, 19);
            this.materialLabel12.TabIndex = 56;
            this.materialLabel12.Text = "Upah Lembur";
            // 
            // materialLabel13
            // 
            this.materialLabel13.AutoSize = true;
            this.materialLabel13.Depth = 0;
            this.materialLabel13.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel13.Location = new System.Drawing.Point(442, 565);
            this.materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel13.Name = "materialLabel13";
            this.materialLabel13.Size = new System.Drawing.Size(77, 19);
            this.materialLabel13.TabIndex = 57;
            this.materialLabel13.Text = "Gaji Bersih";
            // 
            // txtGajiBersih
            // 
            this.txtGajiBersih.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGajiBersih.Depth = 0;
            this.txtGajiBersih.Enabled = false;
            this.txtGajiBersih.Font = new System.Drawing.Font("Roboto", 12F);
            this.txtGajiBersih.Location = new System.Drawing.Point(572, 554);
            this.txtGajiBersih.MaxLength = 50;
            this.txtGajiBersih.MouseState = MaterialSkin.MouseState.OUT;
            this.txtGajiBersih.Multiline = false;
            this.txtGajiBersih.Name = "txtGajiBersih";
            this.txtGajiBersih.Size = new System.Drawing.Size(127, 36);
            this.txtGajiBersih.TabIndex = 58;
            this.txtGajiBersih.Text = "";
            this.txtGajiBersih.UseTallSize = false;
            // 
            // textBoxContextMenuStrip1
            // 
            this.textBoxContextMenuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.textBoxContextMenuStrip1.Depth = 0;
            this.textBoxContextMenuStrip1.MouseState = MaterialSkin.MouseState.HOVER;
            this.textBoxContextMenuStrip1.Name = "textBoxContextMenuStrip1";
            this.textBoxContextMenuStrip1.Size = new System.Drawing.Size(123, 160);
            // 
            // Gaji
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 758);
            this.Controls.Add(this.txtGajiBersih);
            this.Controls.Add(this.materialLabel13);
            this.Controls.Add(this.materialLabel12);
            this.Controls.Add(this.txtUpahLembur);
            this.Controls.Add(this.txtGajiPokok);
            this.Controls.Add(this.materialLabel11);
            this.Controls.Add(this.txtJmlhAnak);
            this.Controls.Add(this.materialLabel10);
            this.Controls.Add(this.txtNamaKaryawan);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.materialListView2);
            this.Controls.Add(this.materialListView1);
            this.Controls.Add(this.materialFloatingActionButton2);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.materialFloatingActionButton1);
            this.Controls.Add(this.materialButton3);
            this.Controls.Add(this.materialButton2);
            this.Controls.Add(this.materialButton1);
            this.Controls.Add(this.txtTahun);
            this.Controls.Add(this.txtBulan);
            this.Controls.Add(this.txtGajiLembur);
            this.Controls.Add(this.txtTotalTunjangan);
            this.Controls.Add(this.txtPajak);
            this.Controls.Add(this.txtJumlahLembur);
            this.Controls.Add(this.txtGajiKotor);
            this.Controls.Add(this.txtIDKaryawan);
            this.Controls.Add(this.materialLabel9);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.materialLabel7);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.materialLabel2);
            this.DrawerWidth = 150;
            this.Name = "Gaji";
            this.Text = "Gaji";
            this.Load += new System.EventHandler(this.z);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialTextBox txtIDKaryawan;
        private MaterialSkin.Controls.MaterialTextBox txtGajiKotor;
        private MaterialSkin.Controls.MaterialTextBox txtJumlahLembur;
        private MaterialSkin.Controls.MaterialTextBox txtPajak;
        private MaterialSkin.Controls.MaterialTextBox txtTotalTunjangan;
        private MaterialSkin.Controls.MaterialTextBox txtGajiLembur;
        private System.Windows.Forms.DateTimePicker txtBulan;
        private System.Windows.Forms.DateTimePicker txtTahun;
        private MaterialSkin.Controls.MaterialButton materialButton1;
        private MaterialSkin.Controls.MaterialButton materialButton2;
        private MaterialSkin.Controls.MaterialButton materialButton3;
        private MaterialSkin.Controls.MaterialFloatingActionButton materialFloatingActionButton1;
        private MaterialSkin.Controls.MaterialTextBox txtSearch;
        private MaterialSkin.Controls.MaterialFloatingActionButton materialFloatingActionButton2;
        private MaterialSkin.Controls.MaterialListView materialListView1;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader nama_karyawan;
        private System.Windows.Forms.ColumnHeader alamat;
        private System.Windows.Forms.ColumnHeader no_hp;
        private System.Windows.Forms.ColumnHeader tempat_lahir;
        private System.Windows.Forms.ColumnHeader tanggal_lahir;
        private System.Windows.Forms.ColumnHeader jenis_kelamin;
        private System.Windows.Forms.ColumnHeader id_jabatan;
        private System.Windows.Forms.ColumnHeader status;
        private System.Windows.Forms.ColumnHeader jumlah_anak;
        private System.Windows.Forms.ColumnHeader username;
        private System.Windows.Forms.ColumnHeader password;
        private MaterialSkin.Controls.MaterialListView materialListView2;
        private System.Windows.Forms.ColumnHeader idjab;
        private System.Windows.Forms.ColumnHeader id_karyawan;
        private System.Windows.Forms.ColumnHeader gaji_kotor;
        private System.Windows.Forms.ColumnHeader jumlah_lembur;
        private System.Windows.Forms.ColumnHeader pajak;
        private System.Windows.Forms.ColumnHeader total_tunjangan;
        private System.Windows.Forms.ColumnHeader gaji_bersih;
        private System.Windows.Forms.ColumnHeader bulan;
        private System.Windows.Forms.ColumnHeader tahun;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialTextBox txtNamaKaryawan;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialTextBox txtJmlhAnak;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialTextBox txtGajiPokok;
        private MaterialSkin.Controls.MaterialTextBox txtUpahLembur;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private MaterialSkin.Controls.MaterialTextBox txtGajiBersih;
        private System.Windows.Forms.ColumnHeader nama_jabatan;
        private System.Windows.Forms.ColumnHeader jumlah_gaji_pokok;
        private System.Windows.Forms.ColumnHeader upah_lembur;
        private MaterialSkin.Controls.TextBoxContextMenuStrip textBoxContextMenuStrip1;
    }
}