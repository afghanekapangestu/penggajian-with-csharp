﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Globalization;

namespace Penggajaian
{
    public partial class Gaji : MaterialForm
    {
        string database = "server = localhost; database=aplikasi_karyawan;uid=root; pwd=''";
        public MySqlConnection koneksi;
        public MySqlCommand command;

        public MySqlDataAdapter adp;
        public MySqlDataReader sdr;



        public Gaji()
        {
            InitializeComponent();

            Sizable = false;


            // Create a material theme manager and add the form to manage (this)
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;

            // Configure color schema
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue400, Primary.Blue500,
                Primary.Blue500, Accent.LightBlue200,
                TextShade.WHITE
            );
        }

        private void BacaGaji()
        {
            string sql = "SELECT * FROM gaji";
            koneksi = new MySqlConnection(database);
            koneksi.Open();
            command = new MySqlCommand(sql, koneksi);
            sdr = command.ExecuteReader();

            while (sdr.Read())
            {
                string[] row = { sdr["id"].ToString(), sdr["id_karyawan"].ToString(), sdr["gaji_kotor"].ToString(), sdr["jml_lembur"].ToString(), sdr["pajak"].ToString(), sdr["total_tunjangan"].ToString(), sdr["gaji_bersih"].ToString(), sdr["bulan"].ToString(), sdr["tahun"].ToString() };
                var listviewitem = new ListViewItem(row);
                materialListView2.Items.Add(listviewitem);
            }
        }

        private void Gaji_Load(object sender, EventArgs e)
        {
           
        }

        private void BacaKaryawan()
        {
            string sql = "SELECT * FROM `karyawan` INNER JOIN jabatan ON karyawan.id_jabatan = jabatan.id";
            koneksi = new MySqlConnection(database);
            koneksi.Open();
            command = new MySqlCommand(sql, koneksi);
            sdr = command.ExecuteReader();

            while (sdr.Read())
            {
                //string[] row = new string[sdr.FieldCount];
                //int[] toskip = { 13 };
                //for (int i = 0; i < sdr.FieldCount; i++)
                //{
                //    bool skipga = false;
                //    foreach (int a in toskip)
                //    {
                //        if (i == a)
                //        {
                //            skipga = true;

                //        }
                //    }
                //    if (skipga)
                //    {
                //        continue;

                //    }

                //    row[i] = sdr.GetString(i);
                //}
                string[] row = { sdr["id"].ToString(), sdr["nama_karyawan"].ToString(), sdr["alamat"].ToString(), sdr["no_hp"].ToString(), sdr["tempat_lahir"].ToString(), sdr["tanggal_lahir"].ToString(), sdr["jenis_kelamin"].ToString(), sdr["id_jabatan"].ToString(), sdr["status"].ToString(), sdr["jml_anak"].ToString(), sdr["username"].ToString(), sdr["password"].ToString(), sdr["nama_jabatan"].ToString(), sdr["jml_gaji_pokok"].ToString(), sdr["upah_lembur"].ToString() };


                var listviewitem = new ListViewItem(row);
                materialListView1.Items.Add(listviewitem);

            }
        }
        private void materialFloatingActionButton1_Click(object sender, EventArgs e)
        {
            Menu mn = new Menu();
            this.Hide();
            mn.ShowDialog();
            this.Close();
        }

        private void materialTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public Boolean Query(string query)
        {
            koneksi = new MySqlConnection(database);
            try
            {
                koneksi.Open();
                command = new MySqlCommand(query, koneksi);
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ewin)
            {
                MessageBox.Show(ewin.Message);
            }
            finally
            {
                koneksi.Close();
            }
            return false;
        }

        //private void BacaData()
        //{
        //    string query = "SELECT * FROM "
        //}

        private void MaterialListView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void searchKaryawan()
        {
            materialListView1.Items.Clear();
            string sql = "SELECT * FROM `karyawan` INNER JOIN jabatan ON karyawan.id_jabatan = jabatan.id WHERE nama_karyawan LIKE '%" + txtSearch.Text + "%'";

            koneksi = new MySqlConnection(database);
            koneksi.Open();
            command = new MySqlCommand(sql, koneksi);
            sdr = command.ExecuteReader();

            while (sdr.Read())
            {
                string[] row = { sdr["id"].ToString(), sdr["nama_karyawan"].ToString(), sdr["alamat"].ToString(), sdr["no_hp"].ToString(), sdr["tempat_lahir"].ToString(), sdr["tanggal_lahir"].ToString(), sdr["jenis_kelamin"].ToString(), sdr["id_jabatan"].ToString(), sdr["status"].ToString(), sdr["jml_anak"].ToString(), sdr["username"].ToString(), sdr["password"].ToString() };
                var listviewitem = new ListViewItem(row);
                materialListView1.Items.Add(listviewitem);
            }
        }

        private void MaterialFloatingActionButton2_Click(object sender, EventArgs e)
        {
            searchKaryawan();
        }

        private void MaterialListView1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (materialListView1.SelectedItems.Count > 0)
            {
                ListViewItem item = materialListView1.SelectedItems[0];


            }
        }

        private void TxtBulan_ValueChanged(object sender, EventArgs e)
        {

        }

        private void UseClear()
        {
            foreach (Control control in this.Controls)
            {
                if (control is MaterialTextBox)
                {
                    MaterialTextBox textbox = control as MaterialTextBox;
                    textbox.Clear();
                }
            }

            foreach (Control control in this.Controls)
            {
                if (control is MaterialComboBox)
                {
                    MaterialComboBox combobox = control as MaterialComboBox;
                    combobox.SelectedItem = null;
                }
            }

        }

        private void materialListView1_SelectedIndexChanged_2(object sender, EventArgs e)
        {
            if (materialListView1.SelectedItems.Count > 0)
            {
                double tunjangan_anak = 0;
                ListViewItem item = materialListView1.SelectedItems[0];
                txtIDKaryawan.Text = item.SubItems[0].Text;
                txtNamaKaryawan.Text = item.SubItems[1].Text;
                txtJmlhAnak.Text = item.SubItems[9].Text;
                txtGajiPokok.Text = item.SubItems[13].Text;
                txtUpahLembur.Text = item.SubItems[14].Text;
                txtJumlahLembur.Enabled = true;

                if (Convert.ToInt32(item.SubItems[9].Text) == 1)
                {
                    tunjangan_anak = 0.15;
                }
                else if (Convert.ToInt32(item.SubItems[9].Text) == 2)
                {
                    tunjangan_anak = 0.30;
                }
                else if (Convert.ToInt32(item.SubItems[9].Text) == 3)
                {
                    tunjangan_anak = 0.45;
                }
                else if (Convert.ToInt32(item.SubItems[9].Text) == 4)
                {
                    tunjangan_anak = 0.60;
                }
                else if (Convert.ToInt32(item.SubItems[9].Text) == 5)
                {
                    tunjangan_anak = 0.75;
                }
                double total_tunjangan = Convert.ToInt32(item.SubItems[13].Text) * tunjangan_anak;
                txtTotalTunjangan.Text = total_tunjangan.ToString() ;
                txtGajiKotor.Text = (Convert.ToInt32(item.SubItems[13].Text) + total_tunjangan).ToString();

                //txtGajiKotor.Text = gaji_kotor.ToString();
                txtPajak.Text = (Convert.ToInt32(item.SubItems[13].Text) * 0.10).ToString();


            }
            else
            {
                UseClear();
            }
        }

        private void txtJumlahLembur_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtJumlahLembur_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtJumlahLembur.Text == null)
                {
                    txtJumlahLembur.Text = "0";
                }
                int gaji_kotor = Convert.ToInt32(txtGajiKotor.Text);
                int gaji_lembur = Convert.ToInt32(txtUpahLembur.Text) * Convert.ToInt32
                    (txtJumlahLembur.Text);
                int pajak = Convert.ToInt32(txtPajak.Text);
                txtGajiLembur.Text = gaji_lembur.ToString();
                //int total_tunjangan = Convert.ToInt32(txtGajiKotor.Text) + Convert.ToInt32(txtGajiLembur.Text);
                //txtTotalTunjangan.Text = total_tunjangan.ToString();

                txtGajiBersih.Text = (gaji_kotor + gaji_lembur - pajak ).ToString();

            }
        }

        private void materialButton1_Click(object sender, EventArgs e)
        {
            string bulan = txtBulan.Value.ToString("MM");
            string tahun = txtTahun.Value.ToString("yyyy");

            string sql = "INSERT INTO gaji VALUES ('','" + txtIDKaryawan.Text + "','" + txtGajiKotor.Text + "','" + txtJumlahLembur.Text + "','" + txtPajak.Text + "','" + txtTotalTunjangan.Text + "','" + txtGajiBersih.Text + "','" + bulan + "','" + tahun + "')";

            if (Query(sql))
            {
                materialListView2.Items.Clear();
                BacaGaji();
                UseClear();
                MessageBox.Show("Data Berhasil disimpan");
            }
            else
            {
                MessageBox.Show("Data tidak berhasil disimpan");
            }
        }

        private void materialButton2_Click(object sender, EventArgs e)
        {
            string bulan = txtBulan.Value.ToString("MM");
            string tahun = txtTahun.Value.ToString("yyyy");

            string sql = "UPDATE gaji SET gaji_kotor='" + txtGajiKotor.Text + "',jml_lembur='" + txtJumlahLembur.Text + "',pajak='" + txtPajak.Text + "',total_tunjangan='" + txtTotalTunjangan.Text + "',gaji_bersih='" + txtGajiBersih.Text + "',bulan='" + bulan + "',tahun='" + tahun + "' WHERE id_karyawan = '" + txtIDKaryawan.Text + "'";

            if (Query(sql))
            {
                materialListView2.Items.Clear();
                BacaGaji();
                UseClear();
                MessageBox.Show("Data Berhasil diedit");
            }
            else
            {
                MessageBox.Show("Data tidak berhasil diedit");
            }
        }

        private void materialButton3_Click(object sender, EventArgs e)
        {
            string sql = "DELETE FROM gaji WHERE id_karyawan = '" + txtIDKaryawan.Text + "'";

            if (Query(sql))
            {
                materialListView2.Items.Clear();
                BacaGaji();
                UseClear();
                MessageBox.Show("Data Berhasil dihapus");
            }
            else
            {
                MessageBox.Show("Data tidak berhasil dihapus");
            }
        }

        private void materialListView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (materialListView2.SelectedItems.Count > 0)
            {
                ListViewItem item = materialListView2.SelectedItems[0];

                txtIDKaryawan.Text = item.SubItems[1].Text;
                txtGajiKotor.Text = item.SubItems[2].Text;
                txtJumlahLembur.Text = item.SubItems[3].Text;
                txtPajak.Text = item.SubItems[4].Text;
                txtTotalTunjangan.Text = item.SubItems[5].Text;
                txtGajiBersih.Text = item.SubItems[6].Text;
                String bulan = item.SubItems[7].Text;
                DateTime time_bulan = DateTime.ParseExact(bulan, "MM", null);
                txtBulan.Value = time_bulan;
                String tahun = item.SubItems[8].Text;
                DateTime time_tahun = DateTime.ParseExact(tahun, "yyyy", null);
                txtBulan.Value = time_tahun;
                txtJumlahLembur.Enabled = false;


            }
            else
            {
                UseClear();
            }
        }

        private void TxtTotalTunjangan_TextChanged(object sender, EventArgs e)
        {

        }

        private void z(object sender, EventArgs e)
        {
            txtBulan.Format = DateTimePickerFormat.Custom;
            txtBulan.CustomFormat = "MMMM";
            txtTahun.Format = DateTimePickerFormat.Custom;
            txtTahun.CustomFormat = "yyyy";

            //command = new MySqlCommand("SELECT * karyawan");
            //koneksi.Open();
            //sdr = command.ExecuteReader();
            //AutoCompleteStringCollection mycol = new AutoCompleteStringCollection();

            BacaKaryawan();
            BacaGaji();
        }
    }
}
