﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Globalization;

namespace Penggajaian
{
    public partial class Jabatan : MaterialForm
    {
        string database = "server = localhost; database=aplikasi_karyawan;uid=root; pwd=''";
        public MySqlConnection koneksi;
        public MySqlCommand command;

        public MySqlDataAdapter adp;
        public MySqlDataReader sdr;

        public Jabatan()
        {
            InitializeComponent();
            bacaData();



        // Create a material theme manager and add the form to manage (this)
        MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;

            // Configure color schema
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue400, Primary.Blue500,
                Primary.Blue500, Accent.LightBlue200,
                TextShade.WHITE
            );
        }

        public Boolean Query(string query)
        {
            koneksi = new MySqlConnection(database);
            try
            {
                koneksi.Open();
                command = new MySqlCommand(query, koneksi);
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ewin)
            {
                MessageBox.Show(ewin.Message);
            }
            finally
            {
                koneksi.Close();
            }
            return false;
        }

        private void bacaData()
        {
            string sql = "SELECT * FROM jabatan";
            koneksi = new MySqlConnection(database);
            koneksi.Open();
            command = new MySqlCommand(sql, koneksi);
            sdr = command.ExecuteReader();

            while (sdr.Read())
            {
                string[] row = { sdr["id"].ToString(), sdr["nama_jabatan"].ToString(), sdr["jml_gaji_pokok"].ToString(), sdr["upah_lembur"].ToString() };
                var listviewitem = new ListViewItem(row);
                materialListView1.Items.Add(listviewitem);
            }

        }



        private void Jabatan_Load(object sender, EventArgs e)
        {

        }

        private void MaterialLabel2_Click(object sender, EventArgs e)
        {

        }

        private void MaterialLabel1_Click(object sender, EventArgs e)
        {

        }
        private void UseClear()
        {
            foreach (Control control in this.Controls)
            {
                if (control is MaterialTextBox)
                {
                    MaterialTextBox textbox = control as MaterialTextBox;
                    textbox.Clear();
                }
            }
        }

        private void MaterialButton1_Click(object sender, EventArgs e)
        {
            string query = "INSERT INTO jabatan VALUES ('','"+txtNamaJabatan.Text+"','"+txtJumlahGajiPokok.Text+"','"+txtUpahLembur.Text+"')";

            if (this.Query(query))
            {
                MessageBox.Show("Data Berhasil ditambahkan");
                materialListView1.Items.Clear();
                bacaData();
                UseClear();

            }
            else
            {
                MessageBox.Show("Data Tidak Berhasil ditambahkan");
            }
        }

        private void MaterialButton2_Click(object sender, EventArgs e)
        {
            delete();
            UseClear();
        }

        private void delete()
        {
            string query = "DELETE FROM jabatan WHERE id='" + txtID.Text + "'";

            if (Query(query))
            {
                MessageBox.Show("Data Berhasil dihapus");
                materialListView1.Items.Clear();
                bacaData();
                UseClear();

            }
            else
            {
                MessageBox.Show("Data tidak berhasil dihapus");
            }
        }

        private void MaterialTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void MaterialFloatingActionButton1_Click(object sender, EventArgs e)
        {
            Menu mn = new Menu();
            this.Hide();
            mn.ShowDialog();
            this.Close();
        }

        private void MaterialButton3_Click(object sender, EventArgs e)
        {
            string query = "UPDATE jabatan SET nama_jabatan ='"+txtNamaJabatan.Text+"',jml_gaji_pokok='"+txtJumlahGajiPokok.Text+"',upah_lembur='"+txtUpahLembur.Text+"' WHERE id='"+txtID.Text+"'";

            if (Query(query))
            {
                MessageBox.Show("Berhasil di Update");
                materialListView1.Items.Clear();
                bacaData();
                UseClear();
            }
            else
            {
                MessageBox.Show("Tidak Berhasil di Update");
            }
        }

        private void MaterialListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(materialListView1.SelectedItems.Count > 0)
            {
                ListViewItem item = materialListView1.SelectedItems[0];
                txtID.Text = item.SubItems[0].Text;
                txtNamaJabatan.Text = item.SubItems[1].Text;
                txtJumlahGajiPokok.Text = item.SubItems[2].Text;
                txtUpahLembur.Text = item.SubItems[3].Text;
            }
            else
            {
                UseClear();
            }
        }
    }
}
