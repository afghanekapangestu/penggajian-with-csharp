﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Penggajaian
{
    public partial class Login : MaterialForm
    {
        
        public Login()
        {
            InitializeComponent();
            Sizable = false;
            SaveUnamePass();

            // Create a material theme manager and add the form to manage (this)
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;

            // Configure color schema
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue400, Primary.Blue500,
                Primary.Blue500, Accent.LightBlue200,
                TextShade.WHITE
            );



        }

        private void SaveUnamePass()
        {
            if (Properties.Settings.Default.username != string.Empty)
            {
                txtUsername.Text = Properties.Settings.Default.username;
                txtPassword.Text = Properties.Settings.Default.password;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SaveUnamePass();

            if (Properties.Settings.Default.status == true) {
                checkRemember.Checked = true;
            }
        }


        private void MaterialLabel2_Click(object sender, EventArgs e)
        {

        }

        private void MaterialSingleLineTextField2_Click(object sender, EventArgs e)
        {

        }

        




        private void LoginAdmin()
        {
            string koneksi = "server=localhost;database=aplikasi_karyawan;uid=root;pwd=''";
            MySqlConnection mc = new MySqlConnection(koneksi);
            try
            {
                mc.Open();
                MySqlDataAdapter da = new MySqlDataAdapter("SELECT * FROM admin WHERE username='" + txtUsername.Text + "' AND password='" + txtPassword.Text + "'", koneksi);

                DataTable dt = new DataTable();

                da.Fill(dt);

                if (dt.Rows.Count > 0)
                {


                    //string username = dt.Rows[0][1].ToString();
                    //MessageBox.Show("Hai "+username);
                    if (checkRemember.Checked)
                    {
                        Properties.Settings.Default.username = txtUsername.Text;
                        Properties.Settings.Default.password = txtPassword.Text;
                        Properties.Settings.Default.status = true;
                        Properties.Settings.Default.Save();
                    }else if (!checkRemember.Checked)
                    {
                        Properties.Settings.Default.username = null;
                        Properties.Settings.Default.password = null;
                        Properties.Settings.Default.status = false;
                        Properties.Settings.Default.Save();
                    }


                    Menu menu = new Menu();
                    this.Hide();
                    menu.ShowDialog();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Masukkan username & password dengan benar", "Message");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
        }




        

       

        private void MaterialButton1_Click(object sender, EventArgs e)
        {
            LoginAdmin();

            
        }

        private void TxtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                MaterialButton1_Click(this, new EventArgs());
            }
        }
    }
}
